import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import Controls from './components/Controls';
import Table from './components/Table';

import { rootReducer } from './store/reducers';

export const
  GET_USER_DATA = 'GET_USER_DATA',
  ACTION_GET_FILTERED_USER_DATA = 'ACTION_GET_FILTERED_USE_DATA';

const store = createStore(rootReducer);

export default class App extends Component {
  render() {
    
    return (
      <div className='text-center buttons'>
        <header className='text-center'>
          <h1>Leaderboard</h1>
        </header>
        <div className='text-center'>
          <Provider store={store}>
            <Controls />
            <Table />
          </Provider>
        </div>
      </div>
    );
  }
}