import React from 'react';

const Age = () => (
  <button
    className='btn btn-primary age'
    name='age'
    onClick={(ev) => {
      console.log('1', ev.target['name']);
    }}
  >
    Age
  </button>
);

export default Age;
