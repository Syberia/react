import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { filterUserData, getUsers } from '../store/actions';

class Controls extends Component {
  render() {
    const { getUsers, filterUserData } = this.props;
    
    return (
      <div className='text-center buttons'>
        <button className='btn btn-primary' onClick={() => { getUsers() }}>Load users</button>
        
        <button
          className='btn btn-primary age'
          name='rank'
          onClick={(ev) => filterUserData(ev.target['name'])}
        >
          Sort by rank
        </button>
        <button
          className='btn btn-primary age'
          name='points'
          onClick={(ev) => filterUserData(ev.target['name'])}
        >
          Sort by points
        </button>
        <button
          className='btn btn-primary age'
          name='name'
          onClick={(ev) => filterUserData(ev.target['name'])}
        >
          Sort by name
        </button>
        <button
          className='btn btn-primary age'
          name='age'
          onClick={(ev) => filterUserData(ev.target['name'])}
        >
          Sort by age
        </button>
      </div>
    )
  }
}

const putDispatchToProps = dispatch => {
  return {
    getUsers: bindActionCreators(getUsers, dispatch),
    filterUserData: bindActionCreators(filterUserData, dispatch)
  }
};

export default connect(
  null,
  putDispatchToProps
)(Controls);