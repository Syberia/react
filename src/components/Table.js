import React, { Component } from 'react';
import { connect } from 'react-redux';

class Table extends Component {
  renderTableHeader(users) {
    let title = Object.keys(users[0]);
    let thead = title.map((key, index) => <th key={index}>{key}</th>);
    return <tr>{thead}</tr>;
  }
  
  renderTableData(users) {
    return users.map((user, index) => {
      let { rank, points, name, age } = user;
      return (
        <tr key={index}>
          <td>{rank}</td>
          <td>{points}</td>
          <td>{name}</td>
          <td>{age}</td>
        </tr>
      )
    })
  }
  
  render() {
    const { users } = this.props;
    if(!users.length) return (<div className='content'>No relevant data</div>);
    
    return (
      <div className='content'>
        <table className='table table-striped'>
          <tbody>
            {this.renderTableHeader(users)}
            {this.renderTableData(users)}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users
});

export default connect(
  mapStateToProps
)(Table);


