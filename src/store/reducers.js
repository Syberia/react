import { GET_USER_DATA, ACTION_GET_FILTERED_USER_DATA } from '../App';

const initialState = {
  users: []
};

export const rootReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_USER_DATA:
      return { ...state,  users: payload };
    case ACTION_GET_FILTERED_USER_DATA:
      return { ...state,  users: sortBy(state, payload) };
  }
  
  return state;
};

const compareBy = (key) => {
  return function (a, b) {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;
    return 0;
  };
};

const sortBy = ({ users }, key) => {
  let filteredUsers = [...users];
  return filteredUsers.sort(compareBy(key));
};