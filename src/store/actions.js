import { GET_USER_DATA, ACTION_GET_FILTERED_USER_DATA } from '../App';

import { userData } from '../data/data';

export const
  getUsers = () => ({
    type: GET_USER_DATA,
    payload: userData
  }),
  filterUserData = (key) => ({
    type: ACTION_GET_FILTERED_USER_DATA,
    payload: key
  });